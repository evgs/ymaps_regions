/**
 * Класс-модель
 * @class
 * @name RegionSelector.Model
 */
RegionSelector.Model = function() {
    this.events = new ymaps.event.Manager();
    this.options = new ymaps.option.Manager({
        preset: this.getDefaults()
    });
    this._monitor = new ymaps.Monitor(this.options);

    this._setupMonitor();
};

/**
 * @lends RegionSelector.Model.prototype
 */
RegionSelector.Model.prototype = {
    /**
     * @contructor
     */
    constructor: RegionSelector.Model,
    /**
     * Используем ymaps.Monitor для наблюдения за изменениями опций модели.
     * @function
     * @private
     * @name RegionSelector.Model._setupMonitor
     */
    _setupMonitor: function() {
        this._monitor
            .add(['country', 'lang', 'quality'], function(newValues, oldValues) {
                /**
                 * Хак для смены языка при смене страны.
                 */
                if (newValues.country !== oldValues.country) {
                    this.options.unset('lang');
                }
                this.load();
            }, this, this.getDefaults());
    },
    /**
     * Отключение мониторинга опций модели.
     * @function
     * @private
     * @name RegionSelector.Model._clearMonitor
     */
    _clearMonitor: function() {
        this._monitor
            .removeAll();
    },
    /**
     * Загружаем данные.
     * @function
     * @name RegionSelector.Model.load
     */
    load: function() {
        ymaps.regions.load(
            this.options.get('country'), {
                lang: this.options.get('lang'),
                quality: this.options.get('quality')
            }
        ).then(
            ymaps.util.bind(this._onDataLoaded, this)
        );
    },
    /**
     * Обработчик загрузки данных.
     * @function
     * @private
     * @name RegionSelector.Model._onDataLoaded
     * @param {Object} data Данные региона.
     */
    _onDataLoaded: function(data) {
        this.events.fire('load', {
            regions: data.geoObjects,
            target: this
        });
    },
    /**
     * Опции модели по-умолчанию.
     * @function
     * @name RegionSelector.Model.getDefaults
     * @returns {Object} Опции модели.
     */
    getDefaults: function() {
        return {
            country: 'RU',
            lang: 'ru',
            quality: 0
        };
    }
};


/**
 * Класс-контрол выбора региона
 * @class
 * @name RegionSelector
 * @param {ymaps.Map} map Карта.
 * @param {jQuery} listContainer Контейнер списка областей.
 * @param {jQuery} optContainer Контейнер для настроек.
 * @param {jQuery} titleContainer Контейнер заголовка.
 */
function RegionSelector(map, listContainer, optContainer, titleContainer) {
    this._model = new RegionSelector.Model();
    this._views = [
        this._mapView = new RegionSelector.MapView(map),
        this._mapMaskView = new RegionSelector.MapMaskView(map)
    ];

    this._attachHandlers();
    this._model.load();
}

/**
 * @lends RegionSelector.prototype
 */
RegionSelector.prototype = {
    /**
     * @constructor
     */
    constructor: RegionSelector,
    /**
     * Добавление обработчиков событий.
     * @function
     * @private
     * @name RegionSelector._attachHandlers
     */
    _attachHandlers: function() {
        this._model.events.add('load', this._onRegionsLoaded, this);
        this._mapView.events.add('itemselected', this._onMapItemSelected, this);

    },
    /**
     * Удаление обработчиков событий.
     * @function
     * @private
     * @name RegionSelector._detachHandlers
     */
    _detachHandlers: function() {
        this._titleView.events.off();
        this._optsView.events.off();
        this._listView.events.off();
        this._mapView.events.remove('regionselected', this._onRegionSelected, this);
        this._model.events.remove('load', this._onRegionsLoaded, this);
    },
    /**
     * Обработчик события загрузки данных о регионах.
     * @function
     * @private
     * @name RegionSelector._onRegionsLoaded
     * @param {ymaps.data.Manager} data Менеджер данных.
     */
    _onRegionsLoaded: function(data) {
        for (var i = 0, len = this._views.length; i < len; i++) {
            this._views[i]
                .clear()
                .render(data);
        }
    },
    /**
     * Обработчик выбора региона на карте.
     * @function
     * @private
     * @name RegionSelector._onMapItemSelected
     * @param {ymaps.data.Manager} e Менеджер данных.
     */
    _onMapItemSelected: function(e) {
        var index = e.get('index');

        this._listView
            .unsetActiveItem()
            .setActiveItem(index)
            .scrollToItem(index);
    },
    /**
     * Обработчик выбора региона в списке.
     * @function
     * @private
     * @name RegionSelector._onListItemSelected
     * @param {jQuery.Event} e Объект-событие.
     */
    _onListItemSelected: function(e) {
        var index = e.itemIndex;

        this._mapView
            .unsetActiveItem()
            .setActiveItem(index)
            .setFocusOnRegion(index);
    },
    _onTitleClick: function(e) {
        this._mapView
            .unsetActiveItem()
            .setFocusOnRegions();

        this._listView
            .unsetActiveItem();
    },
    /**
     * Обработчик смены настроек.
     * @function
     * @private
     * @name RegionSelector._onOptionsChange
     * @param {jQuery.Event} e Объект-событие.
     */
    _onOptionsChange: function(e) {
        this._model.options.set(e.options);
    }
};


/**
 * Класс оверлея маски.
 * @class
 * @name MaskOverlay
 * @param {ymaps.geometry.pixel.Polygon} geometry Пиксельная геометкрия полигона.
 * @param {Object} data Данные.
 * @param {Object} options Опции.
 */
function MaskOverlay(geometry, data, options) {
    MaskOverlay.superclass.constructor.call(this, geometry, data, options);
}

ymaps.ready(function() {
    /**
     * @lends MaskOverlay.prototype
     */
    ymaps.util.augment(MaskOverlay, ymaps.overlay.staticGraphics.Polygon, {
        /**
         * @constructor
         */
        constructor: MaskOverlay,
        /**
         * Перекрываем публичный метод.
         * @function
         * @name MaskOverlay.setGeometry
         * @param {ymaps.geometry.pixel.Polygon} geometry Пиксельная геометрия полигона.
         */
        setGeometry: function(geometry) {
            MaskOverlay.superclass.setGeometry.call(
                this,
                this.getMap() ? this._createGeometry(geometry) : geometry
            );
        },
        /**
         * Создание пиксельной геометрии.
         * @function
         * @private
         * @name MaskOverlay._createGeometry
         * @returns {ymaps.geometry.pixel.Polygon} Пиксельная геометрия полигона.
         */
        _createGeometry: function(geometry) {
            var lineCoordinates = geometry.getCoordinates().slice(0),
                map = this.getMap(),
                center = map.getGlobalPixelCenter(),
                size = map.container.getSize(),
                d = 512;

            lineCoordinates.push([
                [center[0] - size[0] - d, center[1] - size[1] - d],
                [center[0] + size[0] + d, center[1] - size[1] - d],
                [center[0] + size[0] + d, center[1] + size[1] + d],
                [center[0] - size[0] - d, center[1] + size[1] + d],
                [center[0] - size[0] - d, center[1] - size[1] - d]
            ]);

            return new ymaps.geometry.pixel.Polygon(lineCoordinates, 'evenOdd');
        }
    });
});


/**
 * Класс-отображение данных на карте ввиде маски.
 * @class
 * @name RegionSelector.MapMaskView
 * @param {ymaps.Map} map Карта.
 */
RegionSelector.MapMaskView = function(map) {
    this._map = map;
    this._overlay = null;
    this._geometry = null;
};

/**
 * @lends RegionSelector.MapMaskView.prototype
 */
RegionSelector.MapMaskView.prototype = {
    /**
     * @constructor
     */
    constructor: RegionSelector.MapMaskView,
    /**
     * Отображение данных на карте.
     * @function
     * @name RegionSelector.MapMaskView.render
     * @param {ymaps.data.Manager} data Менеджер данных.
     * @returns {RegionSelector.MapMaskView} Возвращает ссылку на себя.
     */
    render: function(data) {
        var coordinates = [];

        data.get('regions')
            .each(function(geoObject) {
                coordinates.push.apply(coordinates, geoObject.geometry.getCoordinates());
            });

        this._createGeometry(coordinates);
        this._createOverlay(this._geometry.getPixelGeometry());
        this._attachHandlers();

        return this;
    },
    /**
     * Удаление данных с карты.
     * @function
     * @name RegionSelector.MapMaskView.clear
     * @returns {RegionSelector.MapMaskView} Возвращает ссылку на себя.
     */
    clear: function() {
        if (this._geometry) {
            this._detachHandlers();
            this._geometry.setMap(null);
            this._overlay.setMap(null);
        }
        this._geometry = this._overlay = null;

        return this;
    },
    /**
     * Добавление обработчиков событий.
     * @function
     * @private
     * @name RegionSelector.MapMaskView._attachHandlers
     */
    _attachHandlers: function() {
        this._geometry.events
            .add('pixelgeometrychange', this._onPixelGeometryChange, this);
        this._map.events
            .add('boundschange', this._onBoundsChange, this);
    },
    /**
     * Удаление обработчиков событий.
     * @function
     * @private
     * @name RegionSelector.MapMaskView._detachHandlers
     */
    _detachHandlers: function() {
        this._map.events
            .remove('boundschange', this._onBoundsChange, this);
        this._geometry.events
            .remove('pixelgeometrychange', this._onPixelGeometryChange, this);
    },
    /**
     * Обработчик события изменения пискельной геометрии.
     * @function
     * @private
     * @name RegionSelector.MapMaskView._onPixelGeometryChange
     * @param {ymaps.data.Manager} e Менеджер данных.
     */
    _onPixelGeometryChange: function(e) {
        this._createOverlay(e.get('newPixelGeometry'));
    },
    /**
     * Обработчик события смены центра/масштаба карты.
     * @function
     * @private
     * @name RegionSelector.MapMaskView._onBoundsChange
     */
    _onBoundsChange: function(e) {
        if (e.get('oldZoom') !== e.get('newZoom')) {
            this._createOverlay(this._geometry.getPixelGeometry());
        }
    },
    /**
     * Создание геометрии типа "Polygon".
     * @function
     * @private
     * @name RegionSelector.MapMaskView._createGeometry
     * @param {Number[][]} coordinates Координаты вершин ломаных, определяющих внешнюю и внутренние границы многоугольника.
     */
    _createGeometry: function(coordinates) {
        this._geometry = new ymaps.geometry.Polygon(coordinates, 'evenOdd', {
            projection: this._map.options.get('projection')
        });
        this._geometry.setMap(this._map);
    },
    /**
     * Создание оверлея.
     * @function
     * @private
     * @name RegionSelector.MapMaskView._createOverlay
     * @param {ymaps.geometry.pixel.Polygon} geometry Пиксельная геометрия полигона.
     */
    _createOverlay: function(geometry) {
        if (!this._overlay) {
            this._overlay = new MaskOverlay(geometry, null, this.getDefaults());
        }
        this._overlay.setMap(this._map);
        this._overlay.setGeometry(geometry);
    },
    /**
     * Опции по-умолчанию.
     * @function
     * @name RegionSelector.MapMaskView.getDefaults
     * @returns {Object} Опции.
     */
    getDefaults: function() {
        return {
            zIndex: 1,
            stroke: false,
            strokeColor: false,
            fillColor: 'fff'
        };
    }
};


/**
 * Класс-отображение регионов на карте.
 * @class
 * @name RegionSelector.MapView
 * @param {ymaps.Map} map Карта.
 */
RegionSelector.MapView = function(map) {
    this._map = map;
    this._regions = null;
    this._activeItem = null;
    this.events = new ymaps.event.Manager();
};

/**
 * @lends RegionSelector.MapView.prototype
 */
RegionSelector.MapView.prototype = {
    /**
     * @constuctor
     */
    constructor: RegionSelector.MapView,
    /**
     * Добавление обработчиков событий.
     * @function
     * @private
     * @name RegionSelector.MapView._attachHandlers
     */
    _attachHandlers: function() {
        this._regions.events.add('click', this._onClick, this);
        this._regions.events.add('mouseenter', this._onMouseEnter, this);
    },
    /**
     * Удаление обработчиков событий.
     * @function
     * @private
     * @name RegionSelector.MapView._detachHandlers
     */
    _detachHandlers: function() {
        this._regions.events.remove('mouseenter', this._onMouseEnter, this);
        this._regions.events.remove('click', this._onClick, this);
    },
    /**
     * Обработчик клика на области региона.
     * @function
     * @private
     * @name RegionSelector.MapView._onClick
     * @param {ymaps.data.Manager} e Менеджер данных.
     */
    _onClick: function(e) {
        var region = e.get('target'),
            index = this._regions.indexOf(region);

        this
            .unsetActiveItem()
            .setActiveItem(index);

        this.events.fire('itemselected', {
            index: index
        });
    },
    /**
     * Отображение данных на карте.
     * @function
     * @name RegionSelector.MapView.render
     * @param {ymaps.data.Manager} data Менеджер данных.
     * @returns {RegionSelector.MapView} Возвращает ссылку на себя.
     */
    render: function(data) {
        this._map.geoObjects.add(
            this._regions = data.get('regions')
        );
        this.setFocusOnRegions();
        this._regions.options.set({
            zIndex: 1,
            zIndexHover: 1,
            fillColor: RegionSelector.MapView.COLOR,
            strokeColor: RegionSelector.MapView.COLOR,
            strokeWidth: 1
        });
        this._attachHandlers();

        return this;
    },
    /**
     * Удаление данных с карты.
     * @function
     * @name RegionSelector.MapView.clear
     * @returns {RegionSelector.MapView} Возвращает ссылку на себя.
     */
    clear: function() {
        if (this._regions) {
            this._detachHandlers();
            this._map.geoObjects.remove(this._regions);
            this._regions = null;
            this._activeItem = null;
        }

        return this;
    },
    /**
     * Выделяем активный регион.
     * @function
     * @name RegionSelector.MapView.setActiveItem
     * @param {Number} index Индекс региона в коллекции.
     * @returns {RegionSelector.MapView} Возвращает ссылку на себя.
     */
    setActiveItem: function(index) {
        var region = this._activeItem = this._regions.get(index);

        region.options.set({
            fillColor: RegionSelector.MapView.SELECTED_COLOR,
            strokeColor: RegionSelector.MapView.SELECTED_COLOR
        });

        return this;
    },
    /**
     * Снимаем выделение активного региона.
     * @function
     * @name RegionSelector.MapView.unsetActiveItem
     * @returns {RegionSelector.MapView} Возвращает ссылку на себя.
     */
    unsetActiveItem: function() {
        if (this._activeItem) {
            this._activeItem.options.set({
                fillColor: RegionSelector.MapView.COLOR,
                strokeColor: RegionSelector.MapView.COLOR
            });
            this._activeItem = null;
        }

        return this;
    },
    /**
     * Выставляем карте область видимости на определенный регион.
     * @function
     * @name RegionSelector.MapView.setFocusOnRegion
     * @param {Number} index Порядковый номер региона в геоколлекции.
     * @returns {RegionSelector.MapView} Возвращает ссылку на себя.
     */
    setFocusOnRegion: function(index) {
        this._map.setBounds(
            this._regions.get(index).geometry.getBounds(), {
                checkZoomRange: true
                    //, duration: 1000
            }
        );

        return this;
    },
    /**
     * Выставляем карте область видимости по всем регионам.
     * @function
     * @name RegionSelector.MapView.setFocusOnRegions
     * @returns {RegionSelector.MapView} Возвращает ссылку на себя.
     */
    setFocusOnRegions: function() {
        this._map.options.set('restrictMapArea', false);

        this._map.setBounds(this._regions.getBounds(), {
            callback: ymaps.util.bind(function() {
             //   this._map.options.set('restrictMapArea', this._map.getBounds())
            }, this)
        });

        return this;
    }
};

/**
 * Цвет областей региона.
 * @static
 * @constant
 */
RegionSelector.MapView.COLOR = 'rgba(81,174,35,1)';
/**
 * Цвет выделенной области.
 * @static
 * @constant
 */
RegionSelector.MapView.SELECTED_COLOR = 'rgba(81,174,35,1)';
