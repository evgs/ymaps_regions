$(document).ready(function() {
    /* Slider on main page mode */
    if ($('.owl-carousel').length > 0) {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 10,
            autoplay: true,
            autoplayTimeout: 2000,
            nav: true,
            loop: true,
            navText: ["<img src='img/left.png'>", "<img src='img/right.png'>"],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });
    }
    /* Catalog mode toggle */
    if ($('.catalog_toggle1').length > 0) {
        $(".catalog_toggle1, .catalog_toggle2").click(function() {
            $(".catalog_toggle1, .catalog_toggle2").removeClass("active");
            $(this).addClass("active");
            if ($(this).hasClass("catalog_toggle1")) {
                $(".row_eshop_list").removeClass("list_mode");
            } else {
                $(".row_eshop_list").addClass("list_mode");
            }
        });
    }

    /* Catalog menu toggle */
    if ($('.catalog_menu_ul').length > 0) {
        $(".catalog_menu_ul").on("click", ".cont01_menu_ul_item", function() {
            $(".catalog_menu_ul_2").hide("fast");
            $(".catalog_menu_ul_2", this).toggle("fast");
        });
    }
});
